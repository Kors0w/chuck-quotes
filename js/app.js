"use strict"

const button = document.getElementById("button")

const quotes = [
    `Un jour Chuck Norris a dit "Va voir la-bas si j'y suis"... et il y était...`,
    `Chuck Norris a déjà compté jusqu'à l'infini. Deux fois.`,
    `Un aigle peut lire un journal à 1400 mètres de distance. Chuck Norris peut tourner la page.`,
    `Chuck Norris est contre les radars automatiques : ça l'éblouit lorsqu'il fait du vélo.`,
    `Quand Google ne trouve pas quelque chose, il demande à Chuck Norris.`,
    `Chuck Norris peut encercler ses ennemis. Tout seul.`,
    `Chuck Norris va régulièrement voir des matchs du PSG pour ne pas oublier ce qu'est la défaite.`,
    `Quand chuck norris marche sur un râteau, le rateau se prend chuck norris dans la gueule.`,
    `Chuck Norris peut monter en bas.`,
    `Au Monopoly Chuck Norris peut acheter la case départ.`
]


function generateQuote() {
        return quotes[Math.floor(Math.random() * quotes.length)]
}

function generateColor() {
    let color = []

    for (let i = 0 ; i < 3 ; i++)
        color.push(Math.floor(Math.random() * 255))

    return `rgb(${color[0]}, ${color[1]}, ${color[2]})`
}


function display() {
    let text = document.getElementById("quote")
    let color = generateColor()
    text.textContent = generateQuote()
    document.querySelector("body").style.backgroundColor = color
    text.style.color = color
}

button.addEventListener("click", display)

display()